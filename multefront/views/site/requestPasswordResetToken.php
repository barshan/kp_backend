
<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Request password reset');
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .card {

        color: red;
    }
    .card-signup {
        width: 42%;
        margin: 0 auto;
        position: relative;
        left: 0;
        margin-top: 15%;
        right: 0;
        bottom: -38%;
    }
</style>

<div class="loan-form signup-form">
    <section class="py-5 sec-loanform ">
        <div class="container">



            <div class="box-shad-light card card-sign card-signup">

                <div class="card-body font">
                    <?php if($success == '1'){

                        echo Yii::t('app', 'Password link is sent to your email, Please check your email.');
                    }
                    else if($success == '2'){
                        echo Yii::t('app', 'Sorry, we are unable to reset password for the provided email address.');
                    }
                    else {

                        $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>


                        <div class="col-12">

                            <?= $form->field($model, 'email', [
                                'template' => '{input}{beginLabel}{labelTitle}{endLabel}<i class="bar"></i>{error}{hint}',
                                'labelOptions' => ['class' => 'control-label']
                            ])->textInput(['id' => 'email', 'class' => 'font']); ?>
                        </div>

                        <div class="col-12">
                            <?= $form->field($model, 'mobile', [
                                'template' => '{input}{beginLabel}{labelTitle}{endLabel}<i class="bar"></i>{error}{hint}',
                                'labelOptions' => ['class' => 'control-label']
                            ])->textInput(['class' => 'font']); ?>
                        </div>

                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('app', '<span>Get Reset Link</span>'), ['class' => 'button m-0 font']) ?>
                        </div>

                        <?php ActiveForm::end();
                    }?>
                </div>
            </div>
        </div>
    </section>
</div>

<script>

    $("input").prop('required',true);
</script>

