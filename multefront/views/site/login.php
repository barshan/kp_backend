
<style>
.site-login{
	background-image: url("image/background.jpg");
}

.log{
	text-align:center;
}
.form {
    margin: 0 auto !important;
    position: relative;
    top: 50%;
    left: 73%;
    width: 337px;
    
}
.form-control, .input-group-addon {
    border-radius: 4px;
}
</style>
<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \multebox\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-login">
    <h1 class="log"><?= Html::encode($this->title) ?></h1>

    <p class="log"><?=Yii::t('app', 'Please fill out the following fields to login')?>:</p>

    <div class="row">
        <div class="col-lg-5">
		<div class="form">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>
				
				<div class="log" style="color:#999;margin:1em 0">
                    <?=Yii::t('app', 'If you forgot your password you can')?> <?= Html::a(Yii::t('app', 'reset it'), ['/site/request-password-reset']) ?>.
                </div>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    <a href="<?=Yii::$app->homeUrl?>site/signup"><button class="btn btn-primary" type="button" value="Register"><span><?=Yii::t('app', 'Register')?></span></button></a>
                </div>

            <?php ActiveForm::end(); ?>
			</div>
        </div>
    </div>
</div>