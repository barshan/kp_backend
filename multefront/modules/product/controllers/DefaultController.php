<?php

namespace multefront\modules\product\controllers;

use Yii;
use multebox\Controller;
use multebox\models\Product;
use multebox\models\Inventory;
use multebox\models\InventoryDetails;
use multebox\models\ProductAttributes;
use multebox\models\ProductSubCategory;
use multebox\models\ProductSubSubCategory;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;


/**
 * Default controller for the `product` module
 */
class DefaultController extends Controller
{
	public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'search' => ['post'],
					'filter' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $lang = $_REQUEST['lang'];
        \Yii::$app->language = $lang;
        $this->layout = '@app/views/layouts/main_new.php';
        $inventoryItemsList = Inventory::find()->where('stock > 0')->orderBy(['id'=>SORT_DESC])->all();
        /*echo '<pre>';
        echo print_r($inventoryItemsList);exit;*/
        return $this->render('products',['inventoryItemsList'=>$inventoryItemsList]);
    }

	public function actionSearch()
    {
        $srch_text = $_REQUEST['searchbox'];
		$escapedstring = str_replace("'", "", $_REQUEST['searchbox']);

		$stringtokens = str_replace(' ', ',', trim(strtolower($escapedstring)));

		$token_array = explode(',', $stringtokens);
		
		$tagstring = '';

		foreach($token_array as $row)
		{
			$tagstring = $tagstring."'".$row."',";
		}
		$tagstring = substr($tagstring, 0, -1).",'".strtolower($escapedstring)."'";
		
		$connection = \Yii::$app->db;

		$query = "select id from tbl_inventory where product_id in 
					(select id from tbl_product where lower(name) like '".strtolower($escapedstring)."%')
					union
					select id from tbl_inventory where id in 
					(select inventory_id from tbl_inventory_tags a, 
					tbl_tags b where b.tag in (".$tagstring.") and b.id = a.tag_id)";


		
		//var_dump($query);exit;

		$mdl = $connection->createCommand($query);

		$result = $mdl->queryAll();
		
		$inventories = "'',";

		foreach($result as $row)
		{
			$inventories = $inventories."'".$row['id']."',";
		}
		$inventories = substr($inventories, 0, -1);
		if($srch_text){
            $inventoryItemsList = Inventory::find()->where("id in (".$inventories.")")->all();
        }else{
            $inventoryItemsList = Inventory::find()->where('stock > 0')->orderBy(['id'=>SORT_DESC])->all();
        }



        return $this->render('products', [
            'inventoryItemsList' => $inventoryItemsList,
			'srch_text'=>$srch_text,
        ]);
    }

    public function actionPricefilter()
    {
        $price_1 = $_REQUEST['price_1'];
        $price_2 = $_REQUEST['price_2'];

            $inventoryItemsList = Inventory::find()->where(['between', 'price', $price_1, $price_2 ])->all();

            /*echo '<pre>';
            echo print_r($inventoryItemsList);exit;*/


        return $this->render('products', [
            'inventoryItemsList' => $inventoryItemsList,
            'price_1'=>$price_1,
            'price_2'=>$price_2,
        ]);
    }

	public function actionDetail()
    {

        if(Yii::$app->user->isGuest) {
            return  Yii::$app->user->loginRequired();

        }
		//var_dump($_POST);exit;
		if(isset($_POST['attribute_value']))
		{
			$names_list = "'@$^',";
			$i=0;
			foreach($_POST['attribute_names'] as $row)
			{
				if($_POST['attribute_value'][$i] != '')
					$names_list = "'".$row."',".$names_list;

				$i++;
			}
			$names_list = substr($names_list, 0, -1);

			if($names_list != "'@$^'")
			{
				
				$connection = \Yii::$app->db;

				/*$result = InventoryDetails::find()
											->alias("a")
											->select("a.inventory_id, group_concat(a.attribute_value SEPARATOR ';') as result")
											->joinWith('productAttributes b')
											->where("b.parent_id=".$_POST['product_id'])
											->andWhere("b.name in (".$names_list.")")
											->andWhere("attribute_id = b.id")
											->groupBy("a.inventory_id")
											->orderBy("inventory_id, attribute_id")->all();
				*/

				$parent_id = Product::findOne($_POST['product_id'])->sub_subcategory_id;

				$query = "SELECT a.inventory_id, group_concat(a.attribute_value order by a.attribute_id SEPARATOR ';' ) as result FROM `tbl_inventory_details` a, `tbl_product_attributes` b 
							WHERE b.parent_id=".$parent_id." and b.name in (".$names_list.")
							and a.attribute_id = b.id
							group by a.inventory_id
							order by a.inventory_id, a.attribute_id";
				//var_dump($query);exit;
				
				$model = $connection->createCommand($query);

				$result = $model->queryAll();

				$search_set = '';

				foreach($_POST['attribute_value'] as $set)
				{
					if($set != '')
						$search_set = $search_set.$set.";";
				}
				$search_set = substr($search_set, 0, -1);
	//var_dump($search_set);exit;
				$inventory_ids = '0,';
				foreach($result as $row)
				{
					if($row['result'] == $search_set)
					{
						$inventory_ids = $row['inventory_id'].",".$inventory_ids;
					}
				}
				$inventory_ids = substr($inventory_ids, 0, -1);

	//var_dump($inventory_ids);exit;
				$itemsList = Inventory::find()
										->where("id in (".$inventory_ids.")")
										->all();
				//var_dump($itemsList);exit;
			}
			else
			{
				$itemsList = Inventory::find()->where("product_id=".$_POST['product_id'])->all();
			}

			return $this->render('listing', [
            'itemsList' => $itemsList,
			'category_id' => '',
			'sub_category_id' => '',
			'sub_subcategory_id' => '',
			'vendor_id' => '',
			'digital' => '',
			'sortfilter' => '',
			'showfilters' => 'false',
			]);
		}
		else
		{
			$inventory = Inventory::findOne($_GET['inventory_id']);
		}

        return $this->render('detail', [
            'inventory' => $inventory
        ]);
    }

    public function actionProdetail(){

        $this->layout = '@app/views/layouts/main_new.php';
        $lang = $_REQUEST['lang'];
        \Yii::$app->language = $lang;

        if(isset($_POST['attribute_value']))
        {

            $names_list = "'@$^',";
            $i=0;
            foreach($_POST['attribute_names'] as $row)
            {
                if($_POST['attribute_value'][$i] != '')
                    $names_list = "'".$row."',".$names_list;
                $i++;
            }
            $names_list = substr($names_list, 0, -1);

            if($names_list != "'@$^'")
            {

                $connection = \Yii::$app->db;

                /*$result = InventoryDetails::find()
                                            ->alias("a")
                                            ->select("a.inventory_id, group_concat(a.attribute_value SEPARATOR ';') as result")
                                            ->joinWith('productAttributes b')
                                            ->where("b.parent_id=".$_POST['product_id'])
                                            ->andWhere("b.name in (".$names_list.")")
                                            ->andWhere("attribute_id = b.id")
                                            ->groupBy("a.inventory_id")
                                            ->orderBy("inventory_id, attribute_id")->all();
                */

                $parent_id = Product::findOne($_POST['product_id'])->sub_subcategory_id;

                $query = "SELECT a.inventory_id, group_concat(a.attribute_value order by a.attribute_id SEPARATOR ';' ) as result FROM `tbl_inventory_details` a, `tbl_product_attributes` b 
							WHERE b.parent_id=".$parent_id." and b.name in (".$names_list.")
							and a.attribute_id = b.id
							group by a.inventory_id
							order by a.inventory_id, a.attribute_id";
                //var_dump($query);exit;

                $model = $connection->createCommand($query);

                $result = $model->queryAll();

                $search_set = '';

                foreach($_POST['attribute_value'] as $set)
                {
                    if($set != '')
                        $search_set = $search_set.$set.";";
                }
                $search_set = substr($search_set, 0, -1);
                //var_dump($search_set);exit;
                $inventory_ids = '0,';
                foreach($result as $row)
                {
                    if($row['result'] == $search_set)
                    {
                        $inventory_ids = $row['inventory_id'].",".$inventory_ids;
                    }
                }
                $inventory_ids = substr($inventory_ids, 0, -1);

                //var_dump($inventory_ids);exit;
                $itemsList = Inventory::find()
                    ->where("id in (".$inventory_ids.")")
                    ->all();
                //var_dump($itemsList);exit;
            }
            else
            {
                $itemsList = Inventory::find()
                    ->where("product_id=".$_POST['product_id'])
                    ->all();
            }

            return $this->render('listing', [
                'itemsList' => $itemsList,
                'category_id' => '',
                'sub_category_id' => '',
                'sub_subcategory_id' => '',
                'vendor_id' => '',
                'digital' => '',
                'sortfilter' => '',
                'showfilters' => 'false',
            ]);
        }
        else
        {
            $inventory = Inventory::findOne($_GET['inventory_id']);
        }

        return $this->render('productdetail', [
            'inventory' => $inventory
        ]);


    }


    public function actionListing()
    {

		/*if($_GET['category_id'] != '' && $_GET['sub_category_id'] != '' && $_GET['sub_subcategory_id'] != '')
		{
			$itemsList = Inventory::find()
								->joinWith('inventoryProducts p')
								->where('p.category_id = '.$_GET['category_id'])
								->andWhere('p.sub_category_id = '.$_GET['sub_category_id'])
								->andWhere('p.sub_subcategory_id = '.$_GET['sub_subcategory_id'])
								->orderBy('stock desc, name')
								->asArray()
								->all();
		}*/
		 if($_GET['s_cat'] != '')
		{
            $inventoryItemsList = Inventory::find()
								->joinWith('inventoryProducts p')
								//->where('p.category_id = '.$_GET['category_id'])
								->where('p.sub_category_id = '.$_GET['s_cat'])
								->orderBy('stock desc, name')
								->all();
		}else if ($_GET['cat'] != ''){

             $inventoryItemsList = Inventory::find()
                 ->joinWith('inventoryProducts p')
                 ->where('p.category_id = '.$_GET['cat'])
                 ->andWhere('stock > 0')
                 ->orderBy('stock desc, name')
                 ->all();
        }
		else{

                 $inventoryItemsList = Inventory::find()->where('stock > 0')->orderBy(['id'=>SORT_DESC])->all();

         }
		/*else
		if($_GET['category_id'] != '')
		{
			$itemsList = Inventory::find()
								->joinWith('inventoryProducts p')
								->where('p.category_id = '.$_GET['category_id'])
								->orderBy('stock desc, name')
								->all();
		}*/

        return $this->render('products', [
            'inventoryItemsList' => $inventoryItemsList,

        ]);



    }

	public function actionFilter()
    {


		if(!empty($_REQUEST['sort']) && $_REQUEST['sort'] != '')
		{
			switch ($_REQUEST['sort'])
			{
				case 'name_asc':
					$orderby = 'product_name';
					$param = SORT_ASC;
					break;

				case 'name_desc':
                    $orderby = 'product_name';
                    $param = SORT_DESC;
					break;

				case 'price_asc':
                    $orderby = 'price';
                    $param = SORT_ASC;
					break;

				case 'price_desc':
                    $orderby = 'price';
                    $param = SORT_DESC;
					break;
			}
		}
		

        $inventoryItemsList = Inventory::find()->where('stock > 0')->orderBy([$orderby => $param])->all();


		/*echo '<pre>';
		echo print_r($inventoryItemsList);
		echo '</pre>';exit;*/



        return $this->render('products', [
            'inventoryItemsList' => $inventoryItemsList,

        ]);
    }

	public function actionAjaxLoadSubCategory(){
		$category_id=!empty($_REQUEST['category_id'])?$_REQUEST['category_id']:'';
		$sub_category_id=!empty($_REQUEST['sub_category_id'])?$_REQUEST['sub_category_id']:'';
		$subcategories = ProductSubCategory::find()->orderBy('name')->where("parent_id=$category_id and active=1")->asArray()->all();
		 return $this->renderPartial('ajax-load-sub-category', [
                'subcategories' => $subcategories,
				'sub_category_id' => $sub_category_id
            ]);
	}

	public function actionAjaxLoadSubSubCategory(){
		$sub_category_id=!empty($_REQUEST['sub_category_id'])?$_REQUEST['sub_category_id']:'';
		$sub_subcategory_id=!empty($_REQUEST['sub_subcategory_id'])?$_REQUEST['sub_subcategory_id']:'';
		$subsubcategories = ProductSubSubCategory::find()->orderBy('name')->where("parent_id=$sub_category_id and active=1")->asArray()->all();
		 return $this->renderPartial('ajax-load-sub-sub-category', [
                'subsubcategories' => $subsubcategories,
				'sub_category_id' => $sub_category_id,
				'sub_subcategory_id' => $sub_subcategory_id
            ]);
	}
}
