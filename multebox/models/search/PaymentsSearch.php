<?php

namespace multebox\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use multebox\models\Payments;
use multebox\models\SubOrder;

/**
 * PaymentsSearch represents the model behind the search form of `app\models\Payments`.
 */
class PaymentsSearch extends Payments
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'order_id', 'payment_method', 'plan_id', 'installment_no', 'installment_amonut', 'other_charges', 'discount', 'grand_total', 'total_paid', 'paid_by', 'mop', 'status', 'refund_reason', 'created_by', 'updated_by'], 'integer'],
            [['due_date', 'payment_date', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payments::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'order_id' => $this->order_id,
            'payment_method' => $this->payment_method,
            'plan_id' => $this->plan_id,
            'due_date' => $this->due_date,
            'installment_no' => $this->installment_no,
            'installment_amonut' => $this->installment_amonut,
            'other_charges' => $this->other_charges,
            'discount' => $this->discount,
            'grand_total' => $this->grand_total,
            'total_paid' => $this->total_paid,
            'paid_by' => $this->paid_by,
            'payment_date' => $this->payment_date,
            'mop' => $this->mop,
            'status' => $this->status,
            'refund_reason' => $this->refund_reason,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        return $dataProvider;
    }

    public function search2($params)
    {
        $query = SubOrder::find()->where(['like','sub_order_status','INP']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            //'customer_id' => $this->customer_id,
            'status' => $this->status,
            // 'order_no' => $this->order_no,
            'plan_id' => $this->plan_id,
            'bank_id' =>$this->plan_id,

        ]);
        return $dataProvider;
    }

    public function pending($params)
    {
        $query = SubOrder::find()->where(['like','sub_order_status','CNF']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            //'customer_id' => $this->customer_id,
            'status' => $this->status,
            // 'order_no' => $this->order_no,
            'plan_id' => $this->plan_id,
            'bank_id' =>$this->plan_id,
            /*'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,*/
        ]);
        return $dataProvider;
    }
}
