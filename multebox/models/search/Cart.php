<?php

namespace multebox\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use multebox\models\Cart as CartModel;

/**
 * Cart represents the model behind the search form about `multebox\models\Cart`.
 */
class Cart extends CartModel
{
    public function rules()
    {
        return [
            [['id', 'inventory_id', 'total_items', 'user_id', 'pay_process_id', 'plan_id','bank_id'], 'integer'],
            [['session_id'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = CartModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'inventory_id' => $this->inventory_id,
            'total_items' => $this->total_items,
            'user_id' => $this->user_id,
            'pay_process_id' => $this->pay_process_id,
            'bank_id' => $this->bank_id,
            'plan_id' => $this->plan_id,
            'added_at' => $this->added_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'session_id', $this->session_id]);

        return $dataProvider;
    }
}
