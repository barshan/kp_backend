<?php

namespace multebox\models;

use Yii;

/**
 * This is the model class for table "payment_plan_template_detail".
 *
 * @property int $id
 * @property int $payment_plan_template_id
 * @property string $type
 * @property string $format
 * @property string $payment_amount
 * @property string $interval
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class PaymentPlanTemplateDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_plan_template_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payment_plan_template_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'created_by'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['type', 'payment_amount'], 'string', 'max' => 15],
            [['format', 'interval'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_plan_template_id' => 'Payment Plan Template ID',
            'type' => 'Type',
            'format' => 'Format',
            'payment_amount' => 'Payment Amount',
            'interval' => 'Interval',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
