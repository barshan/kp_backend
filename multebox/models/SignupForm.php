<?php
namespace multebox\models;

use Yii;
use yii\base\Model;
use multebox\models\search\UserType as UserTypeSearch;
use multebox\models\Contact;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
	public $firstname;
	public $lastname;
    public $mobile;
    public $cnic;
    public $network;
    public $address;
    public $mobotp;
    public $eotp;
    public $mobotpcheck;
    public $eotpcheck;
    public $postcode;
    public $country;
    public $state;
    public $city;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //['username', 'trim'],
            //['username', 'required'],
            //['username', 'unique', 'targetClass' => User::className(), 'message' => 'This username has already been taken.'],
            //['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],

            ['email', 'string', 'max' => 255],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::className(), 'targetAttribute' => ['email'], 'message' =>Yii::t("app","This email address has already been taken") ],
            [['address','state','email','password','firstname','lastname','mobotp'], 'required', 'message' => Yii::t("app","You can't leave this empty.")],

            ['password', 'string', 'min' => 6],



            //['mobotpcheck', 'required','message' => 'Please Verify your Mobile number'],
            //['eotp', 'match','pattern'=>'/^[0-9_-]{6}/','message' => 'Please Enter Valid code'],
            //['eotp', 'required'],
            //['eotp', 'match','pattern'=>'/^[0-9_-]{6}/','message' => 'Please Enter Valid code'],
            //['eotpcheck', 'required','message' => 'Please Verify your email'],
            ['mobile', 'required'],
            ['mobile', 'unique', 'targetClass' => User::className(), 'targetAttribute' => ['mobile'], 'message' => 'This number has already been taken'],
            ['mobile','match','pattern'=>'/^[0-9_-]{11}/','message' => 'Please Enter Valid no. eg:03001234567'],
            ['cnic', 'required'],



            //['network','match','pattern'=>'/^[a-z0-1_-]/','message' => 'You must have Jazz network to avail this service'],
        ];
    }

	public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'firstname' => Yii::t('app', 'First Name'),
            'lastname' => Yii::t('app', 'Last Name'),
            'mobile' => Yii::t('app', 'Mobile'),
            'cnic' => Yii::t('app', 'CNIC'),
            'address' => Yii::t('app', 'Address'),
            'network' => Yii::t('app', 'Mobile Network'),
            'mobotp' => Yii::t('app','Mob Pin'),
            'eotp' => Yii::t('app','Email Pin'),
            'eotpcheck' => Yii::t('app','Email Pin'),
            'mobotpcheck' => Yii::t('app','Mobile Pin'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

		$customer = new Customer();
		$customer->customer_name = $this->mobile;
		$customer->customer_type_id = 2; // Regular Customer
		$customer->added_by_id = 0; //System
		$customer->active = 1;
		$customer->added_at = date('Y-m-d H:i:s');
		if($customer->save())
		{

            $address =new Address();
            $address->address_1 = $this->address;
            $address->address_2 = $this->address;
            $address->country_id = 177;
            $address->city_id = $this->city;
            $address->zipcode = '54000';
            $address->state_id = $this->state;
            $address->entity_type = 'customer';
            $address->entity_id = $customer->id;
            $address->is_primary = 1;
            $address->added_at = date('Y-m-d H:i:s');
            $address->save();

            $contact = new Contact();
            $contact->phone = $this->mobile;
            $contact->email = $this->email;
            $contact->first_name = $this->firstname;
            $contact->last_name = $this->lastname;
            $contact->entity_type = 'customer';
            $contact->entity_id = $customer->id;
            $contact->address_id = $address->id;
            $contact->is_primary = 1;
            $contact->added_at = date('Y-m-d H:i:s');
            $contact->save();

			$user = new User();
			$user->username = $this->mobile;
			$user->email = $this->email;
			$user->first_name = $this->firstname;
			$user->last_name = $this->lastname;
            $user->mobile = $this->mobile;
            $user->cnic = $this->cnic;
            $user->address = $this->address;
            $user->network = $this->network;
			$user->user_type_id = UserTypeSearch::getCompanyUserType('Customer')->id;
			$user->active = 1;
			$user->entity_type = 'customer';
			$user->entity_id = $customer->id;
			$user->setPassword($this->password);
			$user->generateAuthKey();
            $user->added_at = date('Y-m-d H:i:s');


            if($user->save()){
                return $user;
            }
            else{
                echo '<pre>';
                echo print_r($user);
                echo '</pre>';
            }

		}
		else
            echo '<pre>';
        echo print_r($customer);
        echo '</pre>';
        exit;
    }


}
