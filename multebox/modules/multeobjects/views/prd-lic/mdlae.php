<div class="modal fade mdlae">

  <div class="modal-dialog">

    <div class="modal-content">

   	  <div class="modal-header">

        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

        <h4 class="modal-title"><?=Yii::t('app', 'Add Modules to Licence')?></h4>

      </div>

      <div class="modal-body" style="height:350px; overflow:auto">

      	<form action="" method="post">

        <?php Yii::$app->request->enableCsrfValidation = true; ?>

    <input type="hidden" name="_csrf" value="<?php echo $this->renderDynamic('return Yii::$app->request->csrfToken;'); ?>">

      	<table class="table table-bordered table-striped">

        	<thead>

        	<tr>

            	<th>&nbsp;</th>

                <th><?=Yii::t('app', 'Name')?></th>

                <th><?=Yii::t('app', 'Description')?></th>

            </tr>

            </thead>

      	<?php

			if(count($prdMdls) >0){

			foreach($prdMdls as $mrow){

			?>

            	<tr>

                	<td width="10"><input type="checkbox" name="mdls[]" value="<?=$mrow['id']?>"></td> 

                	<td><?=$mrow['mdl_name']?></td>

                    <td><?=$mrow['mdl_desc']?></td>

                </tr>

            <?php } 

			}else{

			?>

            <tr><td colspan="4"><?=Yii::t('app', 'no result')?></td></tr>

            

            <?php } ?>

        </table>

        <div class="form-group">

        	<input type="submit" value="<?=Yii::t('app', 'Add Selected Modules to Licence')?>" class="btn btn-success btn-sm">

        </div>

        </form>

      </div>

   </div>

</div>

</div>