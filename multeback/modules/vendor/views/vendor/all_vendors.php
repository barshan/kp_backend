<style>
	.card {
    box-shadow: 0 0 1px rgba(0,0,0,.125), 0 1px 3px rgba(0,0,0,.2);
    margin-bottom: 1rem;
	background-color:#fff;
}
</style>


<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

use multebox\models\VendorType;
use multebox\models\User;
use multebox\models\search\UserType as UserTypeSearch;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var multebox\models\search\VendorSearch $searchModel
 */

$this->title = Yii::t('app', 'Vendors');
$this->params['breadcrumbs'][] = $this->title;

function statusLabel($status)
{
	if ($status !='1')
	{
		$label = "<span class=\"label label-danger\">".Yii::t('app', 'Inactive')."</span>";
	}
	else
	{
		$label = "<span class=\"label label-primary\">".Yii::t('app', 'Active')."</span>";
	}
	return $label;
}
$status = array('0'=>Yii::t('app', 'Inactive'),'1'=>Yii::t('app', 'Active'));

?>
<div class="vendor-index">
   <!-- <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div> -->
	
   
    <div class="card">
            <div class="card-header" style="padding:12px;">
              	<a class="btn btn-success" href="/emi_latest/multeback/web/vendor/vendor/create"><i class="glyphicon glyphicon-plus"></i> Add</a>

            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
					<th>Id</th>
                  <th>Vendor Name</th>
                  <th>Email</th>
                  <th>Added_at</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
				<?php foreach($data->data as $d=>$val) { ?>
                <tr>
                  <td><?=$val->id?></td>
                  <td><?=$val->vendor_name?></td>
                  <td><?=$val->email?></td>
				  <td><?php echo date('Y-m-d',strtotime($val->createDateTime))?></td>
                  <td> <?php if($val->isActive){echo '<span class="label label-primary">Active</span>';}else{echo '<span class="label label-danger">InActive</span>';}?></td>
                  <td><a href="/emi_latest/multeback/web/vendor/vendor/view?id=2&amp;edit=t" title="Edit"><span class="glyphicon glyphicon-pencil"></span></a>  <a href="/emi_latest/multeback/web/vendor/vendor/delete?id=2" title="Delete" aria-label="Delete" data-pjax="0" data-confirm="Are you sure you want to delete this item?" data-method="post"><span class="glyphicon glyphicon-trash"></span></a> <a href="/emi_latest/multeback/web/vendor/vendor/deactivate?id=2&amp;deactivate=t" title="Deactivate" data-confirm="Are you sure you want to deactivate this vendor?"><span class="glyphicon glyphicon-remove"></span></a></td>
                </tr>
				<?php } ?>
                </tbody>
                <tfoot>
                <tr>
				<th>Id</th>
                  <th>Vendor Name</th>
                  <th>Email</th>
                  <th>Added_at</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
</div>



<script>
  $(function () {
    
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>