<?php



$this->title = Yii::t('app', 'Orders History Reports');
$this->params['breadcrumbs'][] = $this->title;


use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

use multebox\models\CustomerType;
use multebox\models\User;
use multebox\models\search\UserType as UserTypeSearch;
use yii\helpers\ArrayHelper;

?>
<style>
    label {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        float: right !important;
        font-weight: bold;
    }
</style>
<div class="customer-index">
<?php
            $order = \multebox\models\Order::find()->orderBy(['id'=>SORT_DESC])->all();
           /* echo '<pre>';
            echo print_r($order);
            echo '</pre>';*/

 ?>

</div>

<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?=$this->title?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="order-report" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Customer Name</th>
                            <th>Cellphone</th>
                            <th>Email</th>
                            <th>Order ID</th>
                            <th>Item Name</th>
                            <th>Product Color</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Payment Process</th>
                            <th>EMI Plan</th>
                            <th>Per month amount</th>
                            <th>Status</th>
                            <th>View</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($order as $val){
                            $customer = \multebox\models\User::find()->where('entity_id='.$val->customer_id)->one();
                            $encode = json_decode($val->cart_snapshot,true);
                            $inventory = \multebox\models\Inventory::findOne($encode[0]['inventory_id']);
                            /*echo '<pre>';
                            echo print_r($customer);
                            echo '</pre>';*/

                            ?>
                            <?php
                            if($encode[0]['pay_process_id'] == 2){
                                $cash = 'Full Cash';
                            }
                            else{
                                $cash =  'Installment';
                            }
                            if($encode[0]['plan_id'] == 0){
                                $plan = '-----';
                            }
                            else{
                                $plan = \multebox\models\PaymentPlanTemplate::findOne($encode[0]['plan_id']);
                                $plan= $plan->plan_name;
                            } ?>


                            <tr>
                                <td><?= $customer->first_name?></td>
                                <td><?= $customer->mobile?></td>
                                <td><?= $customer->email?></td>
                                <td><?= $val->id?></td>
                                <td><?= $inventory->product_name?></td>
                                <td></td>
                                <td><?= $encode[0]['total_items']?></td>
                                <td><?= $val->total_cost?></td>
                                <td><?=$cash?></td>
                                <td><?=$plan;?></td>
                                <td></td>
                                <td><?=$val->order_status?></td>
                                <td><?=Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                                        Yii::$app->urlManager->createUrl(['/order/sub-order/view-order', 'id' => $model->id]),
                                        ['title' => Yii::t('app', 'View')] )?></td>

                            </tr>


                        <?php } ?>
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

    <!-- /.row -->
</section>

<script>
    $(function () {

        $('#order-report').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            'paging'      : true,
            'lengthChange': false,
            'searching'   : true,
            'ordering'    : false,
            'info'        : true,
            'autoWidth'   : true

        })
    })
</script>