<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\InsuranceRequest */

$this->title = 'Create Insurance Request';
$this->params['breadcrumbs'][] = ['label' => 'Insurance Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insurance-request-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
