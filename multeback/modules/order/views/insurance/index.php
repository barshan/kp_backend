<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\insuranceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Insurances';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insurance-index">


    <?php   $this->render('_search', ['model' => $searchModel]); ?>



    <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'pjax' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'name',
            'mobile',
            //'ntn',
            //'email:email',
            'city',
            'adress',
            //'logo',
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',

            [
                'class' => '\kartik\grid\ActionColumn',
                'template'=>'{view} {update} {delete} ',

                'buttons' => [
                    'delete' => function ($url, $model, $key) {

                        return Html::a('<span class="btn btn-white btn btn-xs">Delete</span>', $url,
                            [
                                'title' => Yii::t('app', 'Delete'),
                                'data-pjax' => '1',
                                'data' => [
                                    'method' => 'post',
                                    'confirm' => Yii::t('app', 'Are You Sure To Delete ?'),
                                    'pjax' => 1,],
                            ]
                        );

                    },
                    'update' => function ($url, $model)
                    {
                        return '<a href="'.Yii::$app->homeUrl.'order/insurance/update?id='.$model->id.'"><span class="btn-white btn btn-xs" >Update</span></a>';
                    } ,
                    'view'=>function($url,$model)
                    {
                        return '<a href="'.Yii::$app->homeUrl.'order/insurance/view?id='.$model->id.'"><span class="btn-white btn btn-xs">View</span></a>';
                    },

                ],
                /*'contentOptions' => function($model)
                {
                    return ['style' => 'max-width:100px !important;'];
                }*/
            ],
        ],
    ]); ?>
</div>
