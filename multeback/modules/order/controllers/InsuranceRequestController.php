<?php

namespace multeback\modules\order\controllers;

use Yii;
use multebox\models\InsuranceRequest;
use multebox\models\search\InsuranceRequestSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * InsuranceRequestController implements the CRUD actions for InsuranceRequest model.
 */
class InsuranceRequestController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InsuranceRequest models.
     * @return mixed
     */

    public function actionIndex()
    {
        $searchModel = new InsuranceRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single InsuranceRequest model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new InsuranceRequest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new InsuranceRequest();

        if ($model->load(Yii::$app->request->post())){

            $oid = $_REQUEST['oid'];
            $cid = $_REQUEST['cid'];
            $model->order_id = $oid;
            $model->customer_id = $cid;
            $model->created_by = Yii::$app->user->identity->getId();
            $model->status = 0;
            $model->created_at = date("Y-m-d H:i:s");
            $model->prod_image = UploadedFile::getInstances($model, 'prod_image');
            if($model->prod_image)
            {
                $directory = Yii::getAlias('@app/drive/insurance_prod_image') . DIRECTORY_SEPARATOR;
                if (!is_dir($directory)) {
                    FileHelper::createDirectory($directory);
                } else
                    foreach ($model->prod_image as $key => $file) {
                        $timestamp1 = date('YmdHis');
                        $ext = pathinfo($file->name, PATHINFO_EXTENSION);
                        $filename = $model->IMEI1.'-'.$timestamp1.$key.'.'.$ext;
                        $file_name1 = str_replace(array(" ", "&"), array("_", "-"), $filename);
                        $filePath = $directory . $file_name1;
                        $model->prod_image = $file_name1;

                        if ($file->saveAs($filePath)) {


                        }
                    }

            }
            if($model->save()){
                return $this->redirect(['index']);
            }

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing InsuranceRequest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing InsuranceRequest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the InsuranceRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InsuranceRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InsuranceRequest::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
