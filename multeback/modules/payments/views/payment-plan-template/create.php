<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var multebox\models\PaymentPlanTemplate $model
 */

$this->title = 'Create Payment Plan Template';
$this->params['breadcrumbs'][] = ['label' => 'Payment Plan Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-plan-template-create">
    <div class="page-header">

    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
