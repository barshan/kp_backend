<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var multebox\models\PaymentPlanTemplate $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="payment-plan-template-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'plan_name')->textInput(['maxlength' => true,'class'=>'form-control summernote rounded']) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'bank_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Banks::find()->all(),'id','name'),['prompt'=>'Select Bank Here','class'=>'form-control summernote rounded']) ?>
        </div>
    </div>
    <?= $form->field($model, 'plan_description')->textarea(['rows' => 6,'class'=>'form-control rounded']) ?>



    <div class="form-group">
        <label class="control-label" for="price-type">Price Type</label>

        <select class="form-control m-b" id="price-type" name="price-type">
            <option value="percentage">Percentage</option>
            <option value="rs">Fixed Amount</option>
        </select>
    </div>

    <div class="table-responsive">
        <table class="table  table-bordered" id="payment_plan_template_generate">
            <thead>
            <tr>
                <th>#</th>
                <th>Payment Amount</th>
                <th>Interval</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>

                    <div class="input-group"><input type="text" name="initial_payment" class="form-control p" onkeypress="return isFloatNumber(this,event)" >

                        <div class="input-group-btn">
                            <span class="btn btn-default p-t">%</span>
                        </div>
                    </div>

                </td>

                <td>
                    Initial Payment
                </td>
                <td></td>
            </tr>
            </tbody>
        </table>
        <table class="table invoice-total">
            <tbody>
            <tr>
                <td><strong>TOTAL :</strong></td>
                <td class="grand-total">0 <span class="p-t">%</span></td>
            </tr>
            </tbody>
        </table>

        <button type="button" class="btn btn-primary btn-xs" id="add-new-table">Add New Row</button>
        <br>
        <br>

    </div>




    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<script>
    $(document).ready(function(){
        var count  = 1;

        var table = $('#payment_plan_template_generate').DataTable({
            //pageLength: 25,
            //responsive: true,
            "bPaginate": false,
            "bFilter": false,
            "bInfo": false,
            dom: '<"html5buttons">',


        });

        $( "#add-new-table" ).click(function() {

            count++;
            table.row.add( [
                count,
                '<div class="input-group"><input type="text" name="payment_amount[]" onkeypress="return isFloatNumber(this,event)" class="form-control p">\n' +
                '\n' +
                '                    <div class="input-group-btn">\n' +
                '                        <span class="btn btn-default p-t">%</span>\n' +
                '                    </div>\n' +
                '                </div>\n',
                ' <div class="row">\n' +
                '                    <div class="col-md-2"><label class="lab-cust">After</label></div>\n' +
                '                    <div class="col-md-4"><input type="number" class="form-control" min="1" name="duration[]" value="1"></div>\n' +
                '                    <div class="col-md-6"><select name="period[]" class="form-control">\n' +
                '                            <option value="days">Day(s)</option>\n' +
                '                            <option value="weeks">Week(s)</option>\n' +
                '                            <option value="months" selected="selected">Month(s)</option>\n' +
                '                            <option value="years">Year(s)</option>\n' +
                '                        </select></div>',
                '<a class="close-link trash">\n' +
                '                    <i class="fa fa-times"></i>\n' +
                '                </a>'
            ] ).draw( false );

        });

        table.on( 'order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        $('#payment_plan_template_generate tbody').on( 'click', '.trash', function (event) {
            count -- ;
            table
                .row( $(this).parents('tr') )
                .remove()
                .draw();
        } );

    });


    $('#price-type').change(function() {
        console.log($(this).val());
        if($(this).val() == 'percentage')
            $('.p-t').text('%');
        else
            $('.p-t').text('Rs');
    });

    function isFloatNumber(item,evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode==46)
        {
            var regex = new RegExp(/\./g)
            var count = $(item).val().match(regex).length;
            if (count > 1)
            {
                return false;
            }
        }
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }

        return true;
    }



    $(document).on('focusout','.p',function() {
        calSum();
    });



    function calSum()
    {
        var total = 0;
        $('.p').each(function(){
            total += parseInt($(this).val());
        });
        $('.grand-total').text(total);
    }






</script>
