<?php

use yii\helpers\Html;
use kartik\file\FileInput;
//use yii\helpers\Html;
use yii\bootstrap\ActiveForm ;

/* @var $this yii\web\View */
/* @var $model app\models\Banks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banks-form">

    <?php $form = ActiveForm::begin(['options' => ['name' => 'multipartforms','enctype' => 'multipart/form-data','enableAjaxValidation' => false, 'novalidate'=>true]]); ?>
	<div class="panel panel-info">
	<div class="panel-heading">
			<h3 class="panel-title"><?php echo Yii::t ( 'app', 'Banks' ); ?></h3>
		</div>
	<div class="panel-body">
    <div class="row">
		
        <div class="col-lg-8">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'name')->textInput(['class'=>'form-control rounded']) ?>
                </div>

                <div class="col-md-6">
                    <?= $form->field($model, 'branch_code')->textInput(['class'=>'form-control rounded']) ?>
                </div>


                <div class="col-md-6">
                    <?= $form->field($model, 'contact_no')->textInput(['class'=>'form-control rounded']) ?>
                </div>

                <div class="col-md-6">
                    <?= $form->field($model, 'email')->textInput(['class'=>'form-control rounded']) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'address')->textarea(['class'=>'form-control rounded']) ?>
                </div>


            </div>

        </div>


        <div class="col-md-4">
            <?= $form->field($model, 'logo')->widget(FileInput::classname(), [
                'options' => ['multiple' => 'false'],
                'pluginOptions' => [
                    'showPreview' => true,
                    'showCaption' => true,
                    'showRemove' => false,
                    'showUpload' => false
                ]
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
	</div>
	</div>

    <?php ActiveForm::end(); ?>
</div>