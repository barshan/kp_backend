<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use multebox\models\Product;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var multebox\models\search\Inventory $searchModel
 */

$this->title = Yii::t('app', 'Inventories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventory-index">
    <!--<div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php /* echo Html::a(Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Inventory',
]), ['create'], ['class' => 'btn btn-success'])*/  ?>
    </p>

    <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'responsiveWrap' => false,
		'pjax' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

        //    'id',
            //'product_id',
			[
                'attribute' => 'product_id',
                'filterType' => GridView::FILTER_SELECT2,
				'label' => Yii::t('app', 'Product Name'),
                'format' => 'raw',
                'width' => '300px',
                'filter' => ArrayHelper::map(Product::find()->orderBy('name')
                    ->where("active=1")
                    ->asArray()
                    ->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'options' => [
                        'placeholder' => Yii::t('app', 'All...')
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ],
                'value' => function ($model, $key, $index, $widget)
                {
					if (isset($model->product_id)) 
					{
						return Product::findOne($model->product_id)->name;
					}
                }
            ],
      //      'vendor_id',
            'stock',
//            'price_type',
            //'attribute_values:ntext', 
			[ 
				'attribute' => 'attribute_values',
				'format' => 'raw',
				'value' => function ($model, $key, $index, $widget)
				{
					if($model->attribute_values == '')
					{
						return "<span class=\"label label-info\">".Yii::t('app', 'NA')."</span>";
					}
					else
					{
						$value = json_decode($model->attribute_values);
						$final='';
						foreach($value as $item)
						{
							if($final == '')
								$final = $item;
							else
								$final = $final."<br/>".$item;
						}
						return $final;
					}
				} 
			],
//            'attribute_price:ntext', 
//            'price', 
//            'discount_type', 
//            'discount', 
//            'shipping_cost', 
//            'added_by_id', 
//            'sort_order', 
//            'added_at', 
//            'updated_at', 

            [
                'class' => 'yii\grid\ActionColumn',
				'template'=>'{update}  {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                            Yii::$app->urlManager->createUrl(['/inventory/inventory/update', 'id' => $model->id, 'edit' => 't']),
                            ['title' => Yii::t('app', 'Edit'),]
                        );
                    }
                ],
            ],
        ],
        'responsive' => true,
        'hover' => true,
        'condensed' => true,
        'floatHeader' => false,

        'panel' => [
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
            'type' => 'info',
            'before' => Yii::$app->params['user_role'] == 'admin'?'':Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),
            'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> '.Yii::t('app', 'Reset List'), ['index'], ['class' => 'btn btn-info']),
            'showFooter' => false
        ],
    ]); Pjax::end(); ?>

</div>
