<?php
namespace multeback\controllers;

use app\helpers\Helper;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use multebox\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use multebox\models\LoginForm;
use multebox\models\SessionVerification;
use multebox\models\SessionDetails;
use multeback\models\PasswordResetRequestForm;
use multeback\models\ResetPasswordForm;
use multeback\models\SignupForm;
use multeback\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */

	public function init()
	{

	}

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'request-password-reset', 'reset-password'],
                'rules' => [
                    [
                        'actions' => ['signup', 'request-password-reset', 'reset-password'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $ch = curl_init('http://15.185.178.149/orders/all'); // Initialise cURL
       // $post = json_encode($objectInv); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_GET, 1); // Specify the request method as POST
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection



         $data = json_decode($result); // Return the received data
       /* echo '<pre>';
        echo print_r($data);
        echo '</pre>';
        exit;*/

        return $this->render('index',['data' =>$data]);
    }


    public function actionOrders()
    {
        $ch = curl_init('http://15.185.178.149/orders/all'); // Initialise cURL
        // $post = json_encode($objectInv); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_GET, 1); // Specify the request method as POST
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result); // Return the received data
        /* echo '<pre>';
         echo print_r($data);
         echo '</pre>';
         exit;*/

        return $this->render('orders',['data' =>$data]);
    }
    public function actionAcceptreport()
    {
        $ch = curl_init('http://15.185.178.149/orders/all'); // Initialise cURL
        // $post = json_encode($objectInv); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_GET, 1); // Specify the request method as POST
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result); // Return the received data
        /* echo '<pre>';
         echo print_r($data);
         echo '</pre>';
         exit;*/

        return $this->render('accepted',['data' =>$data]);
    }
    public function actionRejectreport()
    {
        $ch = curl_init('http://15.185.178.149/orders/all'); // Initialise cURL
        // $post = json_encode($objectInv); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_GET, 1); // Specify the request method as POST
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result); // Return the received data
        /* echo '<pre>';
         echo print_r($data);
         echo '</pre>';
         exit;*/

        return $this->render('rejected',['data' =>$data]);
    }
    public function actionOrdersView()
    {
        $id = $_GET['id'];
        $array['id'] = $id;


        $ch = curl_init('http://15.185.178.149/orders/single'); // Initialise cURL
         $post = json_encode($array); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'  )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result); // Return the received data

        $array['orderId'] = $id;

        $ch = curl_init('http://15.185.178.149/orders/getRejection'); // Initialise cURL
        $post = json_encode($array); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'  )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result1 = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data1 = json_decode($result1); // Return the received data


        return $this->render('order_view',[
            'data' =>$data,
            'data1' =>$data1
        ]);
    }

    public function actionAccept()
    {


        $id = $_REQUEST['idx'];
        $array['id'] = $id;
        $reason = $_REQUEST['reas'];

        $ch = curl_init('http://15.185.178.149/orders/single'); // Initialise cURL
        $post = json_encode($array); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'  )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result);


         $data->data->status = 'accepted';
        unset($data->data->wooCommerceProductId);

        $put = json_encode($data->data);


        $url = 'http://15.185.178.149/orders/update/';
        $ch = curl_init($url); // Initialise cURL
         // Encode the data array into a JSON string
       // $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $put); // Set the putted fields
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch);
        $data = json_decode($result);

        $array2['description'] = $reason;
        $array2['isActive'] = 'Accepted';
        $array2['orderId'] = $id;

        $ch = curl_init('http://15.185.178.149/orders/reject'); // Initialise cURL
        $post = json_encode($array2); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'  )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result);


        if($data->status){
            return 1;
        }else{
            return 0;
        }



        /*echo '<pre>';
        echo print_r($data);
        echo '</pre>';
         exit;*/
        //return $this->render('order_view',['data' =>$data]);

    }
    public function actionReject()
    {
        $id = $_REQUEST['idx'];
         $reason = $_REQUEST['reas'];
        $array['id'] = $id;


        $ch = curl_init('http://15.185.178.149/orders/single'); // Initialise cURL
        $post = json_encode($array); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'  )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result);


        $data->data->status = 'rejected';
        unset($data->data->wooCommerceProductId);

        $put = json_encode($data->data);


        $url = 'http://15.185.178.149/orders/update/';
        $ch = curl_init($url); // Initialise cURL
        // Encode the data array into a JSON string
        // $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $put); // Set the putted fields
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch);
        $data = json_decode($result);

        $array2['description'] = $reason;
        $array2['isActive'] = 'rejected';
        $array2['orderId'] = $id;

        $ch = curl_init('http://15.185.178.149/orders/reject'); // Initialise cURL
        $post = json_encode($array2); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'  )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result);


        if($data->status){
            return 1;
        }else{
            return 0;
        }
    }

    public function actionHold()
    {
        $id = $_REQUEST['idx'];
        $reason = $_REQUEST['reas'];
        $array['id'] = $id;


        $ch = curl_init('http://15.185.178.149/orders/single'); // Initialise cURL
        $post = json_encode($array); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'  )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result);


        $data->data->status = 'On-Hold';
        unset($data->data->wooCommerceProductId);

        $put = json_encode($data->data);

        echo $put.'<br>';
        $url = 'http://15.185.178.149/orders/update/';
        $ch = curl_init($url); // Initialise cURL
        // Encode the data array into a JSON string
        // $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $put); // Set the putted fields
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch);
        $data = json_decode($result);

        $array2['description'] = $reason;
        $array2['isActive'] = 'On-Hold';
        $array2['orderId'] = $id;

        $ch = curl_init('http://15.185.178.149/orders/reject'); // Initialise cURL
        $post = json_encode($array2); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'  )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result);


        if($data->status){
            return 1;
        }else{
            return 0;
        }

    }

    public function actionDispatch()
    {
        $id = $_REQUEST['idx'];
        $reason = $_REQUEST['reas'];
        $array['id'] = $id;


        $ch = curl_init('http://15.185.178.149/orders/single'); // Initialise cURL
        $post = json_encode($array); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'  )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result);


        $data->data->status = 'dispatched';
        unset($data->data->wooCommerceProductId);

        $put = json_encode($data->data);


        $url = 'http://15.185.178.149/orders/update/';
        $ch = curl_init($url); // Initialise cURL
        // Encode the data array into a JSON string
        // $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $put); // Set the putted fields
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch);
        $data = json_decode($result);

        $array2['description'] = $reason;
        $array2['isActive'] = 'dispatched';
        $array2['orderId'] = $id;

        $ch = curl_init('http://15.185.178.149/orders/reject'); // Initialise cURL
        $post = json_encode($array2); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'  )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result);


        if($data->status){
            return 1;
        }else{
            return 0;
        }
    }


    public function actionSendNotification()
    {

        $id = $_REQUEST['idx'];
        $array['id'] = $id;


        $ch = curl_init('http://15.185.178.149/orders/single'); // Initialise cURL
        $post = json_encode($array); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'  )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result);
        $message = $_POST['message'];
        if($_POST['type'] == 'msg'){
            $phone = $data->data->billing->phone ;
            $email = Helper::sendSMS($phone,$message);
            return $this->render('sendnotify', [
                'msg' => 1,
            ]);exit;

        }else if($_POST['type'] == 'mail'){

            $email = $data->data->billing->email ;
            $emailsubject = 'From Financier';

            $email = Helper::sendEmail($email,$message,$emailsubject);
            return $this->render('sendnotify', [
                'msg' => 2,
            ]);exit;

        }else{
            return $this->render('sendnotify');
        }

    }

    public function actionViewDocs()
    {
        $id = $_GET['id'];
        $array['id'] = $id;


        $ch = curl_init('http://15.185.178.149/orders/single'); // Initialise cURL
        $post = json_encode($array); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'  )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result); // Return the received data

        return $this->render('view_docs',['data' =>$data]);
    }

    public function actionViewPayments()
    {
        $id = $_GET['idx'];
        $array['id'] = $id;


        $ch = curl_init('http://15.185.178.149/orders/single'); // Initialise cURL
        $post = json_encode($array); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'  )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result); // Return the received data

        return $this->render('view-payments',['data' =>$data]);
    }

    public function actionViewInsurance()
    {
        $ch = curl_init('http://15.185.178.149/orders/all'); // Initialise cURL
        // $post = json_encode($objectInv); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_GET, 1); // Specify the request method as POST
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result); // Return the received data
        /* echo '<pre>';
         echo print_r($data);
         echo '</pre>';
         exit;*/

        return $this->render('all-insurances',['data' =>$data]);
    }


    public function actionViewSingleInsurance()
    {
        $id = $_GET['id'];
        $array['id'] = $id;


        $ch = curl_init('http://15.185.178.149/orders/single'); // Initialise cURL
        $post = json_encode($array); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'  )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result); // Return the received data

        $array['orderId'] = $id;

        $ch = curl_init('http://15.185.178.149/orders/getRejection'); // Initialise cURL
        $post = json_encode($array); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'  )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result1 = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data1 = json_decode($result1); // Return the received data


        return $this->render('single-insurance',[
            'data' =>$data,
            'data1' =>$data1
        ]);
    }

    public function actionVendorOrders()
    {
        $ch = curl_init('http://15.185.178.149/orders/all'); // Initialise cURL
        // $post = json_encode($objectInv); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_GET, 1); // Specify the request method as POST
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result); // Return the received data
        /* echo '<pre>';
         echo print_r($data);
         echo '</pre>';
         exit;*/

        return $this->render('vendor-orders',['data' =>$data]);
    }


    public function actionViewVendorOrder()
    {
        $id = $_GET['id'];
        $array['id'] = $id;


        $ch = curl_init('http://15.185.178.149/orders/single'); // Initialise cURL
        $post = json_encode($array); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'  )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data = json_decode($result); // Return the received data

        $array['orderId'] = $id;

        $ch = curl_init('http://15.185.178.149/orders/getRejection'); // Initialise cURL
        $post = json_encode($array); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'  )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result1 = curl_exec($ch); // Execute the cURL statement

        curl_close($ch); // Close the cURL connection

        $data1 = json_decode($result1); // Return the received data


        return $this->render('view-vendor-order',[
            'data' =>$data,
            'data1' =>$data1
        ]);
    }


    public function actionProduct(){

        $post = '{
    "createProduct": {
        "name": "Test var prodyct",
        "type": "variable",
        "sku": "test-variable1",
        "description": "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.",
        "short_description": "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.",
        "categories": [{
            "id": 9
        }],
        "images": [{
                "src": "http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_4_front.jpg"
            },
            {
                "src": "http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_4_back.jpg"
            },
            {
                "src": "http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_3_front.jpg"
            },
            {
                "src": "http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_3_back.jpg"
            }
        ],
        "attributes": [{
            "name": "RAM",
            "position": 0,
            "visible": true,
            "variation": true,
            "options": [
                "2GB",
                "4GB",
                "6GB"
            ]
        }]
    },
    "createVariation": {
        "create": [{
                "regular_price": "10.00",
                "sale_price": "7.00",
                "sku": "2GB-Phone",
                "attributes": [{
                    "name": "RAM",
                    "option": "2GB"
                }]
            },
            {
                "regular_price": "15.00",
                "sale_price": "12.00",
                "sku": "4GB-Phone",
                "attributes": [{
                    "name": "RAM",
                    "option": "4GB"
                }]
            }
        ]
    }
}';
        $ch = curl_init('https://demo.kistpay.com/wp-json/kistpay-center/v1/product/createVariableProduct'); // Initialise cURL
        //$post = json_encode($array); // Encode the data array into a JSON string
        $authorization = "Authorization: Bearer 91da882a-033a-3b8e-8c42-6f3ad5f8070e"; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'  )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement


        curl_close($ch); // Close the cURL connection
        echo $result;exit;

    }
    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
		$this->layout = false;

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            /* Create Session History */
            $session_details = new SessionDetails;
            $session_details->user_id = Yii::$app->user->identity->id;
            $session_details->logged_in = time();
            $session_details->location_ip = $_SERVER[REMOTE_ADDR];
            $session_details->session_id = session_id();
            $session_details->save();
            
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        /* Update Session History */
        $session_details = SessionDetails::find()->where(['=', 'user_id', Yii::$app->user->identity->id])->andWhere(['=', 'session_id', session_id()])->one();
        if($session_details)
        {
            $session_details->logged_out = time();
            $session_details->update();
        }

        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
		$this->layout = false;

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
		$this->layout = false;

        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
		$this->layout = false;

        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
