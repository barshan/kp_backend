<?php
/**
 * Created by PhpStorm.
 * User: MCE_Barshan
 * Date: 6/11/2020
 * Time: 11:08 AM
 */


use yii\helpers\Url;
$this->title = Yii::t('app', 'Send Notification');
$this->params['breadcrumbs'][] = $this->title;

?>


<!-- Main content -->
<section class="content">
    <?php if(!empty($msg==1)){
        echo '<div class="alert alert-success">
  <strong>Success!</strong> Mobile SMS Sent Succefully ...
</div>';
    }
    elseif (!empty($msg==2)){
        echo '<div class="alert alert-success">
  <strong>Success! </strong>Email Sent...</div>';

    }
    ?>
    <div class="sub-order-update">

        <!-- <h1>View Vendor Order:  122</h1> -->
        <div class="sub-order-form">
            <form id="w0" class="form-vertical" action="<?=Url::to(['/site/send-notification'])?>?id=<?=$_GET['idx']?>" method="post">
                <input type="hidden" name="_csrf" value="MeqmdbAw38BSplWAKWpwIqizIg2xQSyHiN2lPFCrJvRSqNw-4WO-mWLpENdIXQRq7eFXSMAIee3OkdUPaMdcrQ==">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"> Send Notification</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12">

                            <div class="row">
                                <div class="col-sm-9">

                                    <fieldset id="w2">


                                        <div class="form-group field-suborder-inventory_id required">
                                            <label class="control-label" for="suborder-inventory_id">Select Notification Type</label>
                                            <select name="type" class="form-control">
                                                <option value="msg">Mobile SMS</option>
                                                <option value="mail">Email Notification</option>
                                            </select>
                                            <div class="help-block"></div>
                                        </div>


                                    </fieldset>
                                    <fieldset id="w2">


                                        <div class="form-group field-suborder-inventory_id required">
                                            <label class="control-label" for="suborder-inventory_id">Message to be sent</label>
                                            <textarea class="form-control" name="message" placeholder="Enter your message"></textarea>

                                            <div class="help-block"></div>
                                        </div>


                                    </fieldset>
                                    <button type="submit" class="btn btn-success btn-rounded" data-url="<?= \Yii::$app->urlManager->createUrl(['/site/accept'])?>" data-memid="<?=$data->data->id?>" >
                                        Send
                                    </button>

                                </div>

                                <div class="col-sm-3">


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>






            <!--<a href="/site/orders" <button type="button" class=" btn btn-info btn-rounded" data-url="/emi_latest/multeback/web/customer/customer-documents/view" data-memid="122" data-memordid="145">
                View Documents
            </button> </a>-->

            <div class="pull-right">
            </div>

        </div>

    </div>
</section>



<script>

    $(".rjct").on('click',function(e) {
        var url=$(this).attr('data-url');
        var idx=$(this).attr('data-memid');

        e.preventDefault();
        $.ajax({
            type: "POST",
            url:url,
            data:{
                idx:idx,
            },
            success:function (result) {
                alert('Request Rejected!......');
                location.reload();
            },
            error:function (result) {
                alert('404 Network Connection error');
            }
        });

    });


    $(".accept").on('click',function(e) {

        var url=$(this).attr('data-url');
        var idx=$(this).attr('data-memid');

        e.preventDefault();
        $.ajax({
            type: "POST",
            url:url,
            data:{
                idx:idx,
            },
            success:function (result) {
                alert('Request Successfully Accepted!......');
                location.reload();
            },
            error:function (result) {
                alert('404 Network Connection error');
            }
        });

    });

    $(".hold").on('click',function(e) {

        var url=$(this).attr('data-url');
        var idx=$(this).attr('data-memid');

        e.preventDefault();
        $.ajax({
            type: "POST",
            url:url,
            data:{
                idx:idx,
            },
            success:function (result) {
                alert('Request On-Hold!......');
                location.reload();
            },
            error:function (result) {
                alert('404 Network Connection error');
            }
        });

    });


</script>
