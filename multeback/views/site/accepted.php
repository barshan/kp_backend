<?php
/**
 * Created by PhpStorm.
 * User: MCE_Barshan
 * Date: 6/11/2020
 * Time: 11:08 AM
 */
use yii\helpers\Url;
use multebox\models\search\MulteModel;
use multebox\models\SalesReport;
use multebox\models\Order;
use multebox\models\File;
use multebox\models\OrderStatus;
use multebox\models\PaymentMethods;



$this->title = Yii::t('app', 'Accept Report');
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    label {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        float: right !important;
        font-weight: bold;
    }
</style>
<!-- Main content -->
<section class="content">


    <!-- Main row -->
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">

            <!-- TABLE: LATEST ORDERS -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?=Yii::t('app', 'All Accepted Requests')?></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin" id="orders">
                            <thead>
                            <tr>
                                <th><?=Yii::t('app', 'Order ID')?></th>
                                <th><?=Yii::t('app', 'Date')?></th>
                                <th><?=Yii::t('app', 'Name')?></th>
                                <th><?=Yii::t('app', 'CNIC')?></th>
                                <th><?=Yii::t('app', 'phone')?></th>
                                <th><?=Yii::t('app', 'Status')?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            foreach ($data->data as $k=>$val){


                                if($val->status == 'null' ||empty($val->status) || $val->status == 'pending'){
                                    continue;
                                }else if($val->status == 'rejected' || $val->status == 'On-Hold'){
                                    continue;
                                }
                                switch($val->status)
                                {
                                    case "null":
                                        $status="new";
                                        $label="label-primary";
                                        continue;

                                    case 'accepted':
                                        $status="accepted";
                                        $label="label-success";
                                        break;

                                    case 'rejected':
                                        $label="label-danger";
                                        $status="Rejected";
                                        continue;

                                    default:
                                        $label="label-info";
                                        $status="new";
                                }
                                ?>

                                <tr>
                                    <td><a href="<?=Url::to(['/site/orders-view', 'id' => $val->id])?>"><?=$val->id?></a></td>
                                    <td><?=date("d-m-Y", strtotime($val->billing->createDateTime))?></td>

                                    <td><?=$val->billing->firstName?></td>
                                    <td><?php if($val->billing->cinic == 'null' || empty($val->billing->cinic)){echo '32306-6745354-7';}else{echo $val->billing->cinic;}?></td>
                                    <td><?=$val->billing->phone?></td>
                                    <td><span class="label <?=$label?>"><?php echo $val->status == 'null' ? 'new' : $val->status  ?></span></td>
                                </tr>
                            <?php } ?>


                            <!--<tr>
                                        <td><a href="<?/*=Url::to(['/order/sub-order/view-order', 'id' => $order->id])*/?>"><?/*=$order->id*/?></a></td>
                                        <td><?/*=MulteModel::formatAmount($order->total_cost)*/?></td>
                                        <td><?/*=Yii::t('app', PaymentMethods::getLabelByMethod($order->payment_method))*/?></td>
                                        <td><span class="label <?/*=$label*/?>"><?/*=Yii::t('app', OrderStatus::getLabelByStatus($order->order_status))*/?></span></td>
                                    </tr>-->

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="<?=Url::to(['/order/order/index'])?>" class="btn btn-sm btn-info btn-flat pull-left"><?=Yii::t('app', 'View All Orders')?></a>
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>

    </div>
    <!-- /.row -->
</section>
<!-- /.content -->


<script>
    $(function () {

        $('#orders').DataTable({

            'paging'      : true,
            "pageLength": 30,
            'lengthChange': false,
            'searching'   : true,
            'ordering'    : false,
            'info'        : true,
            'autoWidth'   : true

        })
    })
</script>