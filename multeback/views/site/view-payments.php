<?php
/**
 * Created by PhpStorm.
 * User: MCE_Barshan
 * Date: 6/30/2020
 * Time: 9:24 AM
 */

use yii\helpers\Url;
use multebox\models\search\MulteModel;
use multebox\models\SalesReport;
use multebox\models\Order;
use multebox\models\File;
use multebox\models\OrderStatus;
use multebox\models\PaymentMethods;



$this->title = Yii::t('app', 'Payments Status');
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    label {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        float: right !important;
        font-weight: bold;
    }
</style>
<!-- Main content -->
<section class="content">


    <!-- Main row -->
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">

            <!-- TABLE: LATEST ORDERS -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?=Yii::t('app', 'Payments Status')?></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin" id="orders">
                            <thead>
                            <tr>
                                <th><?=Yii::t('app', 'Order ID')?></th>
                                <th><?=Yii::t('app', 'Name')?></th>
                                <th><?=Yii::t('app', 'CNIC')?></th>
                                <th><?=Yii::t('app', 'EMI #')?></th>

                                <th><?=Yii::t('app', 'Month')?></th>
                                <th><?=Yii::t('app', 'Amount')?></th>

                                <th><?=Yii::t('app', 'Status')?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php



                                ?>


                                <tr class="gradeX">
                                    <td><?=$data->data->id?></td>
                                    <td><?=$data->data->billing->firstName .' '. $data->data->billing->lastName?></td>
                                    <td><?php if($data->data->billing->cinic == 'null' || empty($data->data->billing->cinic)){echo '32306-6745354-7';}else{echo $data->data->billing->cinic;}?></td>
                                    <td >1</td>
                                    <td >July</td>
                                    <td><?php echo empty($data->data->wocomerce->total) ? round(29999 / 3) : round($data->data->wocomerce->total / 3) ?></td>
                                    <td>UnPaid</td>


                                </tr>

                            <tr class="gradeX">
                                <td><?=$data->data->id?></td>
                                <td><?=$data->data->billing->firstName .' '. $data->data->billing->lastName?></td>
                                <td><?php if($data->data->billing->cinic == 'null' || empty($data->data->billing->cinic)){echo '32306-6745354-7';}else{echo $data->data->billing->cinic;}?></td>
                                <td >2</td>
                                <td >August</td>
                                <td><?php echo empty($data->data->wocomerce->total) ? round(29999 / 3) : round($data->data->wocomerce->total / 3) ?></td>
                                <td>UnPaid</td>


                            </tr>

                            <tr class="gradeX">
                                <td><?=$data->data->id?></td>
                                <td><?=$data->data->billing->firstName .' '. $data->data->billing->lastName?></td>
                                <td><?php if($data->data->billing->cinic == 'null' || empty($data->data->billing->cinic)){echo '32306-6745354-7';}else{echo $data->data->billing->cinic;}?></td>
                                <td >3</td>
                                <td >September</td>
                                <td><?php echo empty($data->data->wocomerce->total) ? round(29999 / 3) : round($data->data->wocomerce->total / 3) ?></td>
                                <td>UnPaid</td>


                            </tr>




                            <!--<tr>
                                        <td><a href="<?/*=Url::to(['/order/sub-order/view-order', 'id' => $order->id])*/?>"><?/*=$order->id*/?></a></td>
                                        <td><?/*=MulteModel::formatAmount($order->total_cost)*/?></td>
                                        <td><?/*=Yii::t('app', PaymentMethods::getLabelByMethod($order->payment_method))*/?></td>
                                        <td><span class="label <?/*=$label*/?>"><?/*=Yii::t('app', OrderStatus::getLabelByStatus($order->order_status))*/?></span></td>
                                    </tr>-->

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->

                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>

    </div>
    <!-- /.row -->
</section>
<!-- /.content -->


<script>
    $(function () {

        $('#orders').DataTable({
            "order": [[ 1, "desc" ]],
            'paging'      : false,
            "pageLength": 30,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : false,
            'autoWidth'   : false

        })
    })
</script>