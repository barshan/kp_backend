<?php
/**
 * Created by PhpStorm.
 * User: MCE_Barshan
 * Date: 6/30/2020
 * Time: 9:49 PM
 */


use yii\helpers\Url;
use multebox\models\search\MulteModel;
use multebox\models\SalesReport;
use multebox\models\Order;
use multebox\models\File;
use multebox\models\OrderStatus;
use multebox\models\PaymentMethods;



$this->title = Yii::t('app', 'Vendor Order');
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    label {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        float: right !important;
        font-weight: bold;
    }
</style>
<!-- Main content -->
<section class="content">


    <!-- Main row -->
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">

            <!-- TABLE: LATEST ORDERS -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?=Yii::t('app', 'Vendor Orders')?></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin" id="orders">
                            <thead>
                            <tr>
                                <th><?=Yii::t('app', 'Order ID')?></th>
                                <th><?=Yii::t('app', 'Date')?></th>
                                <th><?=Yii::t('app', 'Name')?></th>
                                <th><?=Yii::t('app', 'CNIC')?></th>
                                <th><?=Yii::t('app', 'phone')?></th>
                                <th><?=Yii::t('app', 'Status')?></th>
                                <th><?=Yii::t('app', 'Actions')?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            foreach ($data->data as $k=>$val){


                                if($val->status == 'null' || empty($val->status) || ($val->status == 'pending' && $val->emiPlan != 'COD') || $val->status == 'rejected' || $val->status == 'On-Hold'){
                                    continue;
                                }

                                ?>

                                <tr>
                                    <td><a href="<?=Url::to(['/site/orders-view', 'id' => $val->id])?>"><?=$val->id?></a></td>
                                    <td><?=date("d-m-Y", strtotime($val->billing->createDateTime))?></td>
                                    <td><?=$val->billing->firstName?></td>
                                    <td><?php if($val->billing->cinic == 'null' || empty($val->billing->cinic)){echo '32306-6745354-7';}else{echo $val->billing->cinic;}?></td>
                                    <td><?=$val->billing->phone?></td>
                                    <td><span class="label label-info"><?php echo $val->status == 'null' ? 'new' : $val->status  ?></span></td>
                                    <td><a href="<?=Url::to(['/site/view-vendor-order', 'id' => $val->id])?>" title="View"><span class="glyphicon glyphicon-eye-open"></span></a>  </td>
                                </tr>
                            <?php } ?>




                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="<?=Url::to(['/site/vendor-orders'])?>" class="btn btn-sm btn-info btn-flat pull-left"><?=Yii::t('app', 'View All Orders')?></a>
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>

    </div>
    <!-- /.row -->
</section>
<!-- /.content -->


<script>
    $(function () {

        $('#orders').DataTable({
            "order": [[ 0, "desc" ]],
            'paging'      : true,
            "pageLength": 30,
            'lengthChange': false,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true

        })
    })
</script>