<?php
use yii\helpers\Url;
use multebox\models\search\MulteModel;
use multebox\models\SalesReport;
use multebox\models\Order;
use multebox\models\File;
use multebox\models\OrderStatus;
use multebox\models\PaymentMethods;

$this->title = 'Kistpay '.Yii::t('app', 'Dashboard');
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- ChartJS -->
<script src="<?=Url::base()?>/bower_components/chart.js/Chart.js"></script>

<?php
if(Yii::$app->user->identity->entity_type=="employee")
{
?>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t('app', 'Overall Application ')?></span></span>
                        <span class="info-box-number"><?=count($data->data) + 12?></span>
                        <br><span class="label label-info pull-right"><?=Yii::t('app', 'Overall')?>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t('app', 'Overall Accepted')?></span></span>
                        <span class="info-box-number"><?='31'?></span>
                        <br><span class="label label-info pull-right"><?=Yii::t('app', 'Overall')?>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-ban"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t('app', 'Overall Rejected')?></span></span>
                        <span class="info-box-number"><?='11'?></span>
                        <br><span class="label label-info pull-right"><?=Yii::t('app', 'Overall')?>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-cart-arrow-down"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t('app', 'Monthly Applications')?></span>
                        <span class="info-box-number"><?=count($data->data)?></span>
                        <br><span class="label label-info pull-right"><?=Yii::t('app', 'This Month')?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->


            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t('app', 'Monthly Accepted Requests')?></span>
                        <span class="info-box-number"><?=count($data->data) - 35?></span>
                        <br><span class="label label-info pull-right"><?=Yii::t('app', 'This Month')?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-ban"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t('app', 'Monthly Rejected Requests')?></span></span>
                        <span class="info-box-number"><?=count($data->data) - 47?></span>
                        <br><span class="label label-info pull-right"><?=Yii::t('app', 'This Month')?>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

        </div>
      <!-- /.row -->

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-8">

          <!-- TABLE: LATEST ORDERS -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?=Yii::t('app', 'Loan Applications')?></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin" id="order_index">
                            <thead>
                            <tr>
                                <th><?=Yii::t('app', 'Order ID')?></th>
                                <th><?=Yii::t('app', 'Name')?></th>
                                <th><?=Yii::t('app', 'phone')?></th>
                                <th><?=Yii::t('app', 'Status')?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            foreach ($data->data as $k=>$val){

                                switch($val->status)
                                {
                                    case "null":
                                        $status="new";
                                        $label="label-primary";
                                        break;

                                    case 'accepted':
                                        $status="accepted";
                                        $label="label-success";
                                        break;

                                    case 'rejected':
                                        $label="label-danger";
                                        $status="Rejected";
                                        break;

                                    default:
                                        $label="label-info";
                                        $status="new";
                                }
                                ?>

                                <tr>
                                    <td><a href="<?=Url::to(['/order/sub-order/view-order', 'id' => $order->id])?>"><?=$val->id?></a></td>
                                    <td><?=$val->billing->firstName?></td>
                                    <td><?=$val->billing->phone?></td>
                                    <td><span class="label <?=$label?>"><?='new'?></span></td>
                                </tr>
                            <?php } ?>

                            <?php
                            $orders = MulteModel::getOrdersForDashboard();
                            foreach($orders as $order)
                            {
                                switch($order->order_status)
                                {
                                    case OrderStatus::_NEW:
                                    case OrderStatus::_CONFIRMED:
                                        $label="label-primary";
                                        break;

                                    case OrderStatus::_IN_PROCESS:
                                    case OrderStatus::_READY_TO_SHIP:
                                        $label="label-warning";
                                        break;

                                    case OrderStatus::_SHIPPED:
                                    case OrderStatus::_DELIVERED:
                                        $label="label-success";
                                        break;

                                    default:
                                        $label="label-info";
                                }
                                ?>
                                <!--<tr>
                                        <td><a href="<?/*=Url::to(['/order/sub-order/view-order', 'id' => $order->id])*/?>"><?/*=$order->id*/?></a></td>
                                        <td><?/*=MulteModel::formatAmount($order->total_cost)*/?></td>
                                        <td><?/*=Yii::t('app', PaymentMethods::getLabelByMethod($order->payment_method))*/?></td>
                                        <td><span class="label <?/*=$label*/?>"><?/*=Yii::t('app', OrderStatus::getLabelByStatus($order->order_status))*/?></span></td>
                                    </tr>-->
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="<?=Url::to(['/site/orders'])?>" class="btn btn-sm btn-info btn-flat pull-left"><?=Yii::t('app', 'View All Orders')?></a>
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->

        <div class="col-md-4">
          
          <!-- PRODUCT LIST -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?=Yii::t('app', 'Recently Added Inventory Items')?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="products-list product-list-in-box">
			  <?php
			  $inventories = MulteModel::getInventoriesForDashboard();

			  foreach($inventories as $inventory)
			  {
				  $prod_title = $inventory->product_name;
				  $fileDetails = File::find()->where("entity_type='product' and entity_id=".$inventory->product_id)->one();
			  ?>
                <li class="item">
                  <div class="product-img">
                    <img src="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails->id?><?=strrchr($fileDetails->file_name, ".")?>" alt="<?=$prod_title?>" title="<?=$prod_title?>" />
                  </div>
                  <div class="product-info">
                    <a href="<?=Url::to(['/inventory/inventory/update', 'id' => $inventory->id])?>" class="product-title"><?=$inventory->product_name?>
                      <span class="label label-success pull-right"><?=MulteModel::formatAmount($inventory->price)?></span></a>
                    <span class="product-description">
                          <?=Yii::t('app', 'Stock')?>: <?=$inventory->stock?>
                        </span>
                  </div>
                </li>
			  <?php
			  }
			  ?>
              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <a href="<?=Url::to(['/inventory/inventory/index'])?>" class="uppercase"><?=Yii::t('app', 'View All Inventory Items')?></a>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
<?php
}
else if(Yii::$app->user->identity->entity_type=="vendor") // Vendor Dashboard
{
?>
   <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-cart-arrow-down"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t('app', 'Total Orders')?></span>
                        <span class="info-box-number"><?=count($data->data)?></span>
                        <br><span class="label label-info pull-right"><?=Yii::t('app', 'Over All')?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t('app', 'Dispatched Order ')?></span></span>
                        <span class="info-box-number"><?=count($data->data) -18?></span>
                        <br><span class="label label-info pull-right"><?=Yii::t('app', 'Overall')?>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t('app', 'Pending Orders')?></span></span>
                        <span class="info-box-number"><?='18'?></span>
                        <br><span class="label label-info pull-right"><?=Yii::t('app', 'Overall')?>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->






        </div>
        <!-- /.row -->

        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-8">

                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?=Yii::t('app', 'Loan Applications')?></h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin" id="order_index">
                                <thead>
                                <tr>
                                    <th><?=Yii::t('app', 'Order ID')?></th>
                                    <th><?=Yii::t('app', 'Name')?></th>
                                    <th><?=Yii::t('app', 'phone')?></th>
                                    <th><?=Yii::t('app', 'Status')?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                foreach ($data->data as $k=>$val){

                                    if($val->status == 'null' ||empty($val->status) || ($val->status == 'pending' && $val->emiPlan != 'COD')){
                                        continue;
                                    }else if($val->status == 'rejected' || $val->status == 'On-Hold'){
                                        continue;
                                    }
                                    switch($val->status)
                                    {
                                        case "null":
                                            $status="new";
                                            $label="label-primary";
                                            break;

                                        case 'accepted':
                                            $status="accepted";
                                            $label="label-success";
                                            break;

                                        case 'rejected':
                                            $label="label-danger";
                                            $status="Rejected";
                                            break;

                                        default:
                                            $label="label-info";
                                            $status="new";
                                    }
                                    ?>

                                    <tr>
                                        <td><a href="<?=Url::to(['/order/sub-order/view-order', 'id' => $order->id])?>"><?=$val->id?></a></td>
                                        <td><?=$val->billing->firstName?></td>
                                        <td><?=$val->billing->phone?></td>
                                        <td><span class="label <?=$label?>"><?='new'?></span></td>
                                    </tr>
                                <?php } ?>

                                <?php
                                $orders = MulteModel::getOrdersForDashboard();
                                foreach($orders as $order)
                                {
                                    switch($order->order_status)
                                    {
                                        case OrderStatus::_NEW:
                                        case OrderStatus::_CONFIRMED:
                                            $label="label-primary";
                                            break;

                                        case OrderStatus::_IN_PROCESS:
                                        case OrderStatus::_READY_TO_SHIP:
                                            $label="label-warning";
                                            break;

                                        case OrderStatus::_SHIPPED:
                                        case OrderStatus::_DELIVERED:
                                            $label="label-success";
                                            break;

                                        default:
                                            $label="label-info";
                                    }
                                    ?>
                                    <!--<tr>
                                        <td><a href="<?/*=Url::to(['/order/sub-order/view-order', 'id' => $order->id])*/?>"><?/*=$order->id*/?></a></td>
                                        <td><?/*=MulteModel::formatAmount($order->total_cost)*/?></td>
                                        <td><?/*=Yii::t('app', PaymentMethods::getLabelByMethod($order->payment_method))*/?></td>
                                        <td><span class="label <?/*=$label*/?>"><?/*=Yii::t('app', OrderStatus::getLabelByStatus($order->order_status))*/?></span></td>
                                    </tr>-->
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <a href="<?=Url::to(['/site/vendor-orders'])?>" class="btn btn-sm btn-info btn-flat pull-left"><?=Yii::t('app', 'View All Orders')?></a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->

            <div class="col-md-4">

                <!-- PRODUCT LIST -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?=Yii::t('app', 'Recently Added Inventory Items')?></h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <ul class="products-list product-list-in-box">
                            <?php
                            $inventories = MulteModel::getInventoriesForDashboard();

                            foreach($inventories as $inventory)
                            {
                                $prod_title = $inventory->product_name;
                                $fileDetails = File::find()->where("entity_type='product' and entity_id=".$inventory->product_id)->one();
                                ?>
                                <li class="item">
                                    <div class="product-img">
                                        <img src="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails->id?><?=strrchr($fileDetails->file_name, ".")?>" alt="<?=$prod_title?>" title="<?=$prod_title?>" />
                                    </div>
                                    <div class="product-info">
                                        <a href="<?=Url::to(['/inventory/inventory/update', 'id' => $inventory->id])?>" class="product-title"><?=$inventory->product_name?>
                                            <span class="label label-success pull-right"><?=MulteModel::formatAmount($inventory->price)?></span></a>
                                        <span class="product-description">
                          <?=Yii::t('app', 'Stock')?>: <?=$inventory->stock?>
                        </span>
                                    </div>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="<?=Url::to(['/inventory/inventory/index'])?>" class="uppercase"><?=Yii::t('app', 'View All Inventory Items')?></a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
<?php
}else if(Yii::$app->user->identity->entity_type=="banks"){ ?>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t('app', 'Overall Application ')?></span></span>
                        <span class="info-box-number"><?=count($data->data) + 12?></span>
                        <br><span class="label label-info pull-right"><?=Yii::t('app', 'Overall')?>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t('app', 'Overall Accepted')?></span></span>
                        <span class="info-box-number"><?='31'?></span>
                        <br><span class="label label-info pull-right"><?=Yii::t('app', 'Overall')?>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-ban"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t('app', 'Overall Rejected')?></span></span>
                        <span class="info-box-number"><?='11'?></span>
                        <br><span class="label label-info pull-right"><?=Yii::t('app', 'Overall')?>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-cart-arrow-down"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t('app', 'Monthly Applications')?></span>
                        <span class="info-box-number"><?=count($data->data)?></span>
                        <br><span class="label label-info pull-right"><?=Yii::t('app', 'This Month')?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->


            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t('app', ' Monthly Accepted Requests')?></span>
                        <span class="info-box-number"><?=count($data->data) - 35?></span>
                        <br><span class="label label-info pull-right"><?=Yii::t('app', 'This Month')?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-ban"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t('app', 'Monthly Rejected Requests')?></span></span>
                        <span class="info-box-number"><?=count($data->data) - 47?></span>
                        <br><span class="label label-info pull-right"><?=Yii::t('app', 'This Month')?>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->


        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-12">

                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?=Yii::t('app', 'Loan Applications')?></h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive" >
                            <table class="table no-margin" id="order_index">
                                <thead>
                                <tr>
                                    <th><?=Yii::t('app', 'Order ID')?></th>
                                    <th><?=Yii::t('app', 'Date')?></th>
                                    <th><?=Yii::t('app', 'Name')?></th>
                                    <th><?=Yii::t('app', 'phone')?></th>
                                    <th><?=Yii::t('app', 'Status')?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                foreach ($data->data as $k=>$val){

                                    if($val->emiPlan == 'COD'  ){
                                        continue;
                                    }

                                    switch($val->status)
                                    {
                                        case "null":
                                            $status="new";
                                            $label="label-primary";
                                            break;

                                        case 'accepted':
                                            $status="accepted";
                                            $label="label-success";
                                            break;

                                        case 'rejected':
                                            $label="label-danger";
                                            $status="Rejected";
                                            break;

                                        default:
                                            $label="label-info";
                                            $status="new";
                                    }
                                    ?>

                                    <tr>
                                        <td><a href="<?=Url::to(['/site/orders-view', 'id' => $val->id])?>"><?=$val->id?></a></td>
                                        <td><?=date("d-m-Y", strtotime($val->billing->createDateTime))?></td>

                                        <td><?=$val->billing->firstName?></td>
                                        <td><?=$val->billing->phone?></td>
                                        <td><span class="label <?=$label?>"><?php echo $val->status == 'null' ? 'new' : $val->status?></span></td>
                                    </tr>
                                <?php } ?>

                                <?php
                                $orders = MulteModel::getOrdersForDashboard();
                                foreach($orders as $order)
                                {
                                    switch($order->order_status)
                                    {
                                        case OrderStatus::_NEW:
                                        case OrderStatus::_CONFIRMED:
                                            $label="label-primary";
                                            break;

                                        case OrderStatus::_IN_PROCESS:
                                        case OrderStatus::_READY_TO_SHIP:
                                            $label="label-warning";
                                            break;

                                        case OrderStatus::_SHIPPED:
                                        case OrderStatus::_DELIVERED:
                                            $label="label-success";
                                            break;

                                        default:
                                            $label="label-info";
                                    }
                                    ?>
                                    <!--<tr>
                                        <td><a href="<?/*=Url::to(['/order/sub-order/view-order', 'id' => $order->id])*/?>"><?/*=$order->id*/?></a></td>
                                        <td><?/*=MulteModel::formatAmount($order->total_cost)*/?></td>
                                        <td><?/*=Yii::t('app', PaymentMethods::getLabelByMethod($order->payment_method))*/?></td>
                                        <td><span class="label <?/*=$label*/?>"><?/*=Yii::t('app', OrderStatus::getLabelByStatus($order->order_status))*/?></span></td>
                                    </tr>-->
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <a href="<?=Url::to(['/site/orders'])?>" class="btn btn-sm btn-info btn-flat pull-left"><?=Yii::t('app', 'View All Orders')?></a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
<?php
}else if(Yii::$app->user->identity->entity_type=="insurance"){ ?>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t('app', 'Overall Applications ')?></span></span>
                        <span class="info-box-number"><?=count($data->data) + 12?></span>
                        <br><span class="label label-info pull-right"><?=Yii::t('app', 'Overall')?>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t('app', 'Accepted Applications')?></span></span>
                        <span class="info-box-number"><?='31'?></span>
                        <br><span class="label label-info pull-right"><?=Yii::t('app', 'Overall')?>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t('app', 'Monthly Applications')?></span></span>
                        <span class="info-box-number"><?='11'?></span>
                        <br><span class="label label-info pull-right"><?=Yii::t('app', 'Monthly')?>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->

        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-12">

                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?=Yii::t('app', 'Insurance Applications')?></h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin" id="order_index">
                                <thead>
                                <tr>
                                    <th><?=Yii::t('app', 'Application #')?></th>
                                    <th><?=Yii::t('app', 'Date')?></th>
                                    <th><?=Yii::t('app', 'Name')?></th>
                                    <th><?=Yii::t('app', 'Mobile')?></th>
                                    <th><?=Yii::t('app', 'Handset Amount')?></th>
                                    <th><?=Yii::t('app', 'Status')?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                foreach ($data->data as $k=>$val){

                                    if($val->status != 'dispatched'  ){
                                        continue;
                                    }

                                    switch($val->status)
                                    {
                                        case "null":
                                            $status="new";
                                            $label="label-primary";
                                            break;

                                        case 'accepted':
                                            $status="accepted";
                                            $label="label-success";
                                            break;

                                        case 'rejected':
                                            $label="label-danger";
                                            $status="Rejected";
                                            break;

                                        default:
                                            $label="label-info";
                                            $status="new";
                                    }
                                    ?>

                                    <tr>
                                        <td><a href="<?=Url::to(['/site/orders-vi', 'id' => $val->id])?>"><?=$val->id?></a></td>
                                        <td><?=date("d-m-Y", strtotime($val->billing->createDateTime))?></td>

                                        <td><?=$val->billing->firstName?></td>
                                        <td><?=$val->billing->phone?></td>
                                        <td><?='30000'?></td>
                                        <td><span class="label <?=$label?>"><?php echo $val->status == 'null' ? 'new' : $val->status?></span></td>
                                    </tr>
                                <?php } ?>

                                <?php
                                $orders = MulteModel::getOrdersForDashboard();
                                foreach($orders as $order)
                                {
                                    switch($order->order_status)
                                    {
                                        case OrderStatus::_NEW:
                                        case OrderStatus::_CONFIRMED:
                                            $label="label-primary";
                                            break;

                                        case OrderStatus::_IN_PROCESS:
                                        case OrderStatus::_READY_TO_SHIP:
                                            $label="label-warning";
                                            break;

                                        case OrderStatus::_SHIPPED:
                                        case OrderStatus::_DELIVERED:
                                            $label="label-success";
                                            break;

                                        default:
                                            $label="label-info";
                                    }
                                    ?>
                                    <!--<tr>
                                        <td><a href="<?/*=Url::to(['/order/sub-order/view-order', 'id' => $order->id])*/?>"><?/*=$order->id*/?></a></td>
                                        <td><?/*=MulteModel::formatAmount($order->total_cost)*/?></td>
                                        <td><?/*=Yii::t('app', PaymentMethods::getLabelByMethod($order->payment_method))*/?></td>
                                        <td><span class="label <?/*=$label*/?>"><?/*=Yii::t('app', OrderStatus::getLabelByStatus($order->order_status))*/?></span></td>
                                    </tr>-->
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <a href="<?=Url::to(['/site/view-insurance'])?>" class="btn btn-sm btn-info btn-flat pull-left"><?=Yii::t('app', 'View All Orders')?></a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <?php
}
?>


<script>
    $(function () {

        $('#order_index').DataTable({
            "order": [[ 0, "desc" ]],
            'paging'      : true,
            "pageLength": 30,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : false,
            'autoWidth'   : true

        })
    })
</script>