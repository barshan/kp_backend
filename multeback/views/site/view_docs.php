<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\helpers\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */



?>
<style>

    .title{
        text-align: center;padding: 15px;font-weight: bold;text-decoration: underline;letter-spacing: 2px;font-size: 25px;
    }
</style>
<div class="customer-view">

    <?php ?>
</div>

<?php
$id = Yii::$app->user->identity->id;
// $query = \multebox\models\CustomerDocuments::find()->where(['=','customer_id',$id])->all();
?>
<div class="wrapper wrapper-content animated fadeInRight">





    <div class="row">
        <div class="col-lg-11">



            <div class="row">
                <?php  ?>

                <div class="col-lg-6">
                    <div class="ibox ">
                        <div class=" text-center p-md">

                            <h4 class="m-b-xxs"><?='Front CNIC'?></h4>
                            <div class="m-t-md">
                                <?php if(empty($data->data->billing->cnicFront)){ ?>
                                <div class="p-lg ">
                                    <embed src="<?=app\helpers\Helper::getBaseUrl()?>drive/customerDocuments/CNIC_front-20190423054027.jpg"
                                           width="350px" height="200px"/>
                                </div>
                                <a href="<?=Helper::getBaseUrl()?>drive/customerDocuments/CNIC_front-20190423054027.jpg"><button type="button" class=" btn btn-success">
                                        Download
                                </button></a>
                                <?php }else { ?>
                                    <div class="p-lg ">
                                        <embed src="<?=$data->data->billing->cnicFront?>"
                                               width="350px" height="200px"/>
                                    </div>
                                    <a href="<?=$data->data->billing->cnicFront?>"><button type="button" class=" btn btn-success">
                                            Download
                                        </button></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ibox ">
                        <div class=" text-center p-md">

                            <h4 class="m-b-xxs"><?='CNIC Back'?></h4>
                            <?php if(empty($data->data->billing->cnicBack)){ ?>
                                <div class="p-lg ">
                                    <embed src="<?=app\helpers\Helper::getBaseUrl()?>drive/customerDocuments/CNIC_front-20190423054027.jpg"
                                           width="350px" height="200px"/>
                                </div>
                                <a href="<?=Helper::getBaseUrl()?>drive/customerDocuments/CNIC_front-20190423054027.jpg"><button type="button" class=" btn btn-success">
                                        Download
                                    </button></a>
                            <?php }else { ?>
                                <div class="p-lg ">
                                    <embed src="<?=$data->data->billing->cnicBack?>"
                                           width="350px" height="200px"/>
                                </div>
                                <a href="<?=$data->data->billing->cnicBack?>"><button type="button" class=" btn btn-success">
                                        Download
                                    </button></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ibox ">
                        <div class=" text-center p-md">

                            <h4 class="m-b-xxs"><?='Utility Bill'?></h4>
                            <?php if(empty($data->data->information->utilityBills)){ ?>
                                <div class="p-lg ">
                                    <embed src="<?=app\helpers\Helper::getBaseUrl()?>drive/customerDocuments/CNIC_front-20190423054027.jpg"
                                           width="350px" height="200px"/>
                                </div>
                                <a href="<?=Helper::getBaseUrl()?>drive/customerDocuments/CNIC_front-20190423054027.jpg"><button type="button" class=" btn btn-success">
                                        Download
                                    </button></a>
                            <?php }else { ?>
                                <div class="p-lg ">
                                    <embed src="<?=$data->data->information->utilityBills?>"
                                           width="350px" height="200px"/>
                                </div>
                                <a href="<?=$data->data->information->utilityBills?>"><button type="button" class=" btn btn-success">
                                        Download
                                    </button></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ibox ">
                        <div class=" text-center p-md">

                            <h4 class="m-b-xxs"><?='Salary Slip/Bank Statement '?></h4>
                            <?php if(empty($data->data->employers->salarySlip)){ ?>
                                <div class="p-lg ">
                                    <embed src="<?=app\helpers\Helper::getBaseUrl()?>drive/customerDocuments/CNIC_front-20190423054027.jpg"
                                           width="350px" height="200px"/>
                                </div>
                                <a href="<?=Helper::getBaseUrl()?>drive/customerDocuments/CNIC_front-20190423054027.jpg"><button type="button" class=" btn btn-success">
                                        Download
                                    </button></a>
                            <?php }else { ?>
                                <div class="p-lg ">
                                    <embed src="<?=$data->data->employers->salarySlip?>"
                                           width="350px" height="200px"/>
                                </div>
                                <a href="<?=$data->data->employers->salarySlip?>"><button type="button" class=" btn btn-success">
                                        Download
                                    </button></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <?php
                ?>
            </div>

        </div>
