<?php
/**
 * Created by PhpStorm.
 * User: MCE_Barshan
 * Date: 6/11/2020
 * Time: 11:08 AM
 */


use yii\helpers\Url;
$this->title = Yii::t('app', 'Vendor Order Details');
$this->params['breadcrumbs'][] = $this->title;
?>


<!-- Main content -->
<section class="content">
    <div class="sub-order-update">

        <!-- <h1>View Vendor Order:  122</h1> -->
        <div class="sub-order-form">
            <form id="w0" class="form-vertical" action="#" method="post">
                <input type="hidden" name="_csrf" value="MeqmdbAw38BSplWAKWpwIqizIg2xQSyHiN2lPFCrJvRSqNw-4WO-mWLpENdIXQRq7eFXSMAIee3OkdUPaMdcrQ==">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"> Order Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12">

                            <div class="row">
                                <div class="col-sm-9">
                                    <fieldset id="w1">
                                        <div class="row">
                                            <div class="col-sm-3">

                                                <div class="form-group field-suborder-sub_order_status required">
                                                    <label class="control-label" for="suborder-sub_order_status">Order Status</label>
                                                    <input type="text" id="suborder-sub_order_status" class="form-control" name="SubOrder[sub_order_status]" value="<?php echo $data->data->status != 'dispatched' ? 'new' : $data->data->status  ?>" disabled maxlength="3" placeholder="Enter Sub Order Status..." aria-required="true">

                                                    <div class="help-block"></div>
                                                </div>
                                            </div>

                                            <div class="col-sm-3">
                                                <div class="form-group field-suborder-total_items required">
                                                    <label class="control-label" for="suborder-total_items">Total Items</label>
                                                    <input type="text" id="suborder-total_items" class="form-control" name="SubOrder[total_items]" value="1" disabled placeholder="Enter Total Items..." aria-required="true">
                                                    <div class="help-block"></div>
                                                </div>
                                            </div>

                                            <div class="col-sm-3">


                                                <div class="form-group field-suborder-total_cost required">
                                                    <label class="control-label" for="suborder-total_cost">Total Cost (PKR)</label>
                                                    <input type="text" id="suborder-total_cost" class="form-control" name="SubOrder[total_cost]" value="<?php echo empty($data->data->wocomerce->total) ? '29,999' : $data->data->wocomerce->total ?>" disabled placeholder=" Total Cost..." aria-required="true">

                                                    <div class="help-block"></div>
                                                </div>

                                            </div>

                                        </div>

                                    </fieldset>
                                    <fieldset id="w2">


                                        <div class="form-group field-suborder-inventory_id required">
                                            <label class="control-label" for="suborder-inventory_id">Product Name & Model</label>
                                            <input type="text" id="suborder-inventory_id" class="form-control" name="SubOrder[inventory_id]" value="<?php echo empty($data->data->wocomerce->productName)  ? 'Infinix Zero 5' : $data->data->wocomerce->productName?>" disabled placeholder="Enter Inventory ID..." aria-required="true">
                                            <div class="help-block"></div>
                                        </div>


                                    </fieldset>
                                    <fieldset id="w2">


                                        <div class="form-group field-suborder-inventory_id required">
                                            <label class="control-label" for="suborder-inventory_id">Customer Full Name</label>
                                            <input type="text" id="suborder-inventory_id" class="form-control" name="cus_name" value="<?=$data->data->billing->firstName .' '. $data->data->billing->lastName?>" disabled placeholder="Enter Inventory ID..." aria-required="true">

                                            <div class="help-block"></div>
                                        </div>


                                    </fieldset>
                                    <fieldset id="w2">


                                        <div class="form-group field-suborder-inventory_id required">
                                            <label class="control-label" for="suborder-inventory_id">Customer CNIC</label>
                                            <input type="text" id="suborder-inventory_id" class="form-control" name="cus_name" value="<?php if($data->data->billing->cinic == 'null' || empty($data->data->billing->cinic)){echo '32306-6745354-7';}else{echo $data->data->billing->cinic;}?>" disabled placeholder="Enter Inventory ID..." aria-required="true">

                                            <div class="help-block"></div>
                                        </div>


                                    </fieldset>
                                    <fieldset id="w2">


                                        <div class="form-group field-suborder-inventory_id required">
                                            <label class="control-label" for="suborder-inventory_id">Home Address</label>
                                            <input type="text" id="suborder-inventory_id" class="form-control" name="cus_name" value="<?php if($data->data->billing->address1 == 'null' || empty($data->data->billing->address1)){echo 'Karachi';}else{echo $data->data->billing->address1;}?>" disabled placeholder="Enter Inventory ID..." aria-required="true">

                                            <div class="help-block"></div>
                                        </div>


                                    </fieldset>
                                    <fieldset id="w2">


                                        <div class="form-group field-suborder-inventory_id required">
                                            <label class="control-label" for="suborder-inventory_id">City</label>
                                            <input type="text" id="suborder-inventory_id" class="form-control" name="cus_name" value="<?php if($data->data->billing->city == 'null' || empty($data->data->billing->address2)){echo 'Karachi';}else{echo $data->data->billing->city;}?>" disabled placeholder="Enter Inventory ID..." aria-required="true">

                                            <div class="help-block"></div>
                                        </div>


                                    </fieldset>
                                    <fieldset id="w2">


                                        <div class="form-group field-suborder-inventory_id required">
                                            <label class="control-label" for="suborder-inventory_id">Phone</label>
                                            <input type="text" id="suborder-inventory_id" class="form-control" name="cus_name" value="<?php if($data->data->billing->phone == 'null' || empty($data->data->billing->phone)){echo '03007865423';}else{echo $data->data->billing->phone;}?>" disabled placeholder="Enter Inventory ID..." aria-required="true">

                                            <div class="help-block"></div>
                                        </div>


                                    </fieldset>
                                    <fieldset id="w5">
                                        <div class="row">




                                            <div class="col-sm-4">


                                                <div class="form-group field-suborder-added_at">
                                                    <label class="control-label" for="suborder-added_at">Order Date & Time</label>
                                                    <input type="text" id="suborder-added_at" class="form-control" name="SubOrder[added_at]" value="<?=date("d-m-Y g:i:s", strtotime($data->data->billing->createDateTime))?>" disabled placeholder="Enter Added At...">

                                                    <div class="help-block"></div>
                                                </div>

                                            </div>

                                        </div>

                                    </fieldset>

                                    <?php if($data->data->status == 'accepted'){ ?>
                                        <button type="button" class="dispatch btn btn-success btn-rounded" data-url="<?= \Yii::$app->urlManager->createUrl(['/site/dispatch'])?>" data-memid="<?=$data->data->id?>" >
                                            Order Dispatch
                                        </button>
                                    <?php }
                                    ?>


                                </div>

                                <div class="col-sm-3">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Item Image</h3>
                                        </div>
                                        <div class="panel-body">

                                            <div class="row">
                                                <div class="col-sm-12 text-center">
                                                    <a target="_blank" href="#"><img src="https://demo.kistpay.com/wp-content/uploads/2020/05/nova-3i1-150x150.jpg" alt="Infinix Zero 5" title="Infinix Zero 5"  style="height: 110px;"/></a>
                                                </div>
                                            </div>

                                        </div> <!-- panel body -->
                                    </div> <!-- panel info -->

                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Address Details</h3>
                                        </div>
                                        <div class="panel-body">

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <b>Shipping Address</b><br>
                                                    <?=$data->data->billing->firstName .' '. $data->data->billing->lastName?><br>
                                                    <?=$data->data->billing->address1?><br>
                                                    <?=$data->data->billing->address2 ?><br>
                                                    <br/>
                                                    <?=$data->data->billing->city ?><br/>
                                                    Email: <?=$data->data->billing->email ?><br/>
                                                    Phone: <?=$data->data->billing->phone ?><br/>
                                                </div>
                                            </div>

                                        </div> <!-- panel body -->
                                    </div> <!-- panel info -->
                                </div>


                                <div class="col-sm-12">
                                    <br><hr><br><br><br>
                                    <p class="lead">Order History</p>
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Status</th>
                                            <th>Comments</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($data1->data as $k=>$val){ ?>
                                            <tr>
                                                <td>
                                                    <?=date('d-m-Y')?>
                                                </td>
                                                <td>
                                                <span class="label label-info">
                                                    <?=$val->isActive?>
                                                </span>
                                                </td>
                                                <td style="text-transform: initial;"><?=$val->description?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>


                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </form>






            <!--<a href="/emi_latest/multeback/web/customer/customer-documents/view" target="_blank"<button type="button" class=" btn btn-info btn-rounded" data-url="/emi_latest/multeback/web/customer/customer-documents/view" data-memid="122" data-memordid="145">
                View Documents
            </button> </a>-->


            <!-- /.col -->

        </div>




    </div>
</section>



<script>

    $(".dispatch").on('click',function(e) {
        var url=$(this).attr('data-url');
        var idx=$(this).attr('data-memid');

        e.preventDefault();
        bootbox.dialog({
            title: "IMEI",
            message: '<div class="form-group">'+
            '<label style="color: white;" class="label-control"  for="usr">IMEI</label>'+
            '<input type="number" name="reason" class="form-control" id="reason" ></input>'+
            '</div>',
            buttons: {
                cancel: {
                    label: "Cancel",
                    className: 'btn-danger',
                },
                ok: {
                    label: "Proceed",
                    className: 'btn-success',
                    callback: function(){
                        var reas = $('#reason').val();

                        $.ajax({
                            type: "GET",
                            url: url,
                            data: {
                                idx: idx,
                                reas: reas

                            },
                            success: function (result) {
                                alert('Order Dispatched..!!');
                                location.reload();
                            },
                            error: function (result) {
                                alert('404 Network Connection error');
                            }
                        });
                    }
                }
            }
        });
        /*$.ajax({
            type: "POST",
            url:url,
            data:{
                idx:idx,
            },
            success:function (result) {

                alert('Request Rejected!......');
                location.reload();
            },
            error:function (result) {
                alert('404 Network Connection error');
            }
        });*/

    });





</script>
